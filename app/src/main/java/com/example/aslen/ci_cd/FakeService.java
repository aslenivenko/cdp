package com.example.aslen.ci_cd;

public class FakeService {

    public boolean returnTrue() {
        return true;
    }

    public boolean returnFalse() {
        return false;
    }

    public int returnFour() {
        return 4;
    }

    public String ReturnHelloWorld() {
        return "Hello World1!!";
    }

}
