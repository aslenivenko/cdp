package com.example.aslen.ci_cd;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FakeServiceTest {


    private FakeService fakeService;

    @Before
    public void setUp() throws Exception {
        fakeService = new FakeService();
    }

    @Test
    public void returnTrue() {
        assertTrue(fakeService.returnTrue());
    }

    @Test
    public void returnFalse() {
        assertFalse(fakeService.returnFalse());
    }

    @Test
    public void returnFour() {
        int expected = 4;

        assertEquals(expected, fakeService.returnFour());
    }

    @Test
    public void returnHelloWorld() {
        String expected = "Hello World1!!";

        assertEquals(expected, fakeService.ReturnHelloWorld());
    }
}